import java.util.Scanner;
public class Roulette{
    public static void main(String[]args){
        RouletteWheel roulette = new RouletteWheel();
        Scanner reader = new Scanner(System.in);
        int betMoney;
        int betNumber;
        
        System.out.println("How much do you want to bet?");
        betMoney = reader.nextInt();
        System.out.println("What number do you want to bet on?");
        betNumber = reader.nextInt();
        roulette.spin();
        
        if(betNumber == roulette.getValue()){
            System.out.println("You won!");
            System.out.println("Money won: " + betMoney*35 + "$");
        }else{
            System.out.println("You lost!");
            System.out.println("Money lost: " + betMoney + "$");
        }

    }
}